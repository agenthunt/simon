import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 140,
    width: 140,
    borderRadius: 70
  }
});

export default class SimonSwitch extends Component {
  render(){
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={[styles.container,   {backgroundColor: this.props.color},
        {opacity: this.props.opacity}]}>
        </View>
      </TouchableOpacity>
    );
  }
}
