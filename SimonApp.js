import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import SimonSwitch from './components/SimonSwitch.js';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 200,
    margin: 20,
    alignSelf: 'stretch'
  },
  startButton: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: 'black',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  startText: {
    color: 'white'
  }
});

function getRandomNumber(max){
  return Math.floor(Math.random() * max);
}

function generateSequenceOfLength(level){
  let res = [];
  for(let i=0;i< level; i++){
    let num = getRandomNumber(4);
    if(i>0){
      while(res[i-1]===num){
        num = getRandomNumber(4);
      }
    }
    res.push(num);
  }
  return res;
}

export default class SimonApp extends Component {
  constructor(){
    super();
    this.state = {
      gameOver: false,
      level: 1,
      currentSequence: [],
      userSequence: [],
      playingSequence:[],
      opacities: [1,1,1,1]
    }
  }
  onStart = () => {
    this.setState({
      gameOver: false
    });
    let currentSequence = generateSequenceOfLength(this.state.level);
    let playingSequence = [];
    for(let i=0;i< currentSequence.length;i++){
      playingSequence[i] = currentSequence[i];
    }
    this.setState({
      currentSequence:currentSequence,
      playingSequence: playingSequence
    });
    setTimeout(this.play, 500);
  }
  play = () => {
    if(this.state.playingSequence.length !== 0 ){
      let opacities = [1, 1, 1, 1];
      let old = this.state.playingSequence;
      let index = old.shift();
      opacities[index] = 0.2;
      this.setState({
        opacities,
        playingSequence: old
      });
      setTimeout(this.play, 1500);
    }
    if(this.state.playingSequence.length === 0){
      setTimeout(() => {
      this.setState({
        opacities: [1,1,1,1]
      });
    }, 1500);
    }
  }
  renderGameOverText(){
    if(this.state.level === 1){
      return (
        <Text style={styles.startText}> Start </Text>
      )
    }
  }
  renderStartButton(){
    return (
      <TouchableOpacity onPress={this.onStart}>
        <View style={styles.startButton}>
          <Text style={styles.startText}>
            {'Level: ' + this.state.level}
          </Text>
          {this.renderGameOverText()}
        </View>
      </TouchableOpacity>
    );
  }
  onSwitchPressed = (index) => {
    let newUserSequence = this.state.userSequence;
    newUserSequence.push(index);
    this.setState({
      userSequence: newUserSequence
    });
    for(let i=0;i<newUserSequence.length;i++){
      if(newUserSequence[i] !== this.state.currentSequence[i]) {
        this.setState({
          gameOver: true,
          level: 1,
          playingSequence: [],
          userSequence: []
        });
        alert('Game Over');
      }else {
        if(newUserSequence.length === this.state.currentSequence.length){
          let oldLevel = this.state.level;
          this.setState({
            level: (oldLevel + 1),
            userSequence: []
          });
          setTimeout(this.onStart, 2000);
        }
      }
    }
  }
  render(){
    return (
      <View style={styles.container}>
      <ScrollView style={{height: 200, marginTop: 20}}>
        <Text>
          {JSON.stringify(this.state)}
        </Text>
      </ScrollView>
        <SimonSwitch color="red" style={styles.red} onPress={this.onSwitchPressed.bind(this, 0)}
        opacity={this.state.opacities[0]}/>
        <View style={styles.horizontal}>
          <SimonSwitch color="green"
            style={styles.red}
            onPress={this.onSwitchPressed.bind(this,1)}opacity={this.state.opacities[1]}/>
          {this.renderStartButton()}
          <SimonSwitch color="yellow"
            style={styles.red}
            onPress={this.onSwitchPressed.bind(this,2)}opacity={this.state.opacities[2]}/>
        </View>
        <SimonSwitch color="blue"
          style={styles.red}
          onPress={this.onSwitchPressed.bind(this,3)}
          opacity={this.state.opacities[3]}/>
      </View>
    );
  }
}
